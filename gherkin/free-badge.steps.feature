Feature: manualBadge

Background:
  Given active realm is __test__realm_badges__

  Scenario: Prepare user and realm for test
    Given the User with name __test__admin_badges__ exists

  Scenario: Should create realm
    When they create new realm as BADGES_REALM
      """
        {
          "name": "__test__realm_badges__",
          "type": "public",
          "isFree": true,
          "sitsLimit": 100
        }
      """
    Then they expect the response code to equal 201

  Scenario: Should create badge for realm
    When they create new badges as FREE_BADGE
      """
        {
          "name": "manualBadge",
          "description": "test",
          "icon": "icon.jpg",
          "capacity": null,
          "type": "manual",
          "members": []
        }
      """
    Then they expect the response code to equal 201

  Scenario: Should get available manual badge for realm
    Given the User with name __test__admin_badges__ exists
    When they get badges/<FREE_BADGE._id> as FREE_BADGE
    Then they expect the response code to equal 200
    Then they expect value <FREE_BADGE.name> to equal manualBadge
    Then they expect value <FREE_BADGE.type> to equal manual
    # And they expect the response body to equal badge.schema.dto

  
  Scenario: [N] Should not get manual badge as available for assigning
    Given the User with name __test__admin_badges__ exists
    When they get badges/available as AVAILABLE_BADGE
    Then they expect the response code to equal 200
    Then they not expect value <FREE_BADGE._id> to be in <AVAILABLE_BADGE.items> by _id


  Scenario: [N] Should not create badge for realm without name
    Given active realm is __test__realm_badges__
    Given the User with name __test__admin_badges__ exists
    When they create new badges as FREE_BADGE_withoutName
      """
        {
          "description": "test",
          "icon": "icon.jpg",
          "capacity": null,
          "type": "manual",
          "members": []
        }
      """
    Then they expect the response code to equal 400

  Scenario: [N] Should not create badge for realm without type
    Given active realm is __test__realm_badges__
    Given the User with name __test__admin_badges__ exists
    When they create new badges as FREE_BADGE_withoutType
      """
        {
          "name": "withoutTypeBadge",
          "description": "test",
          "icon": "icon.jpg",
          "capacity": null,
          "members": []
        }
      """
    Then they expect the response code to equal 400

  Scenario: [N] Should not create badge for realm without icon
    Given active realm is __test__realm_badges__
    Given the User with name __test__admin_badges__ exists
    When they create new badges as FREE_BADGE_withoutIcon
      """
        {
          "name": "withoutTypeBadge",
          "description": "test",
          "capacity": null,
          "type": "manual",
          "members": []
        }
      """
    Then they expect the response code to equal 400

  Scenario: [N] Should not get invalid badges for realm by id
    Given the User with name __test__admin_badges__ exists
    When they get badges/<FREE_BADGE_withoutName._id> as FREE_BADGE_withoutName
    Then they not expect value <FREE_BADGE_withoutName._id> to be in <FREE_BADGE_withoutName> by _id

  